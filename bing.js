// ==UserScript==
// @name         BING Auto Search JS
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://www.bing.com/
// @grant        none
// @require      http://code.jquery.com/jquery-3.1.1.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.js
// ==/UserScript==

(function() {
    'use strict';
    console.log('Auto Search Started');
    $('#sb_form_q').val('javascript');
	$('#sb_form_go').click();
})();