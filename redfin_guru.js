// ==UserScript==
// @name         Redfin Rent Calculation  (Testing) 
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  CODE CODE AND CODE !
// @author       You
// @match        https://www.redfin.com/WA/*
// @grant        none
// @require      http://code.jquery.com/jquery-3.1.1.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.js
// ==/UserScript==

setTimeout(function() {
    'use strict';
    
    console.log('Gross Rent Multiplier Started...');
    var price=$('span[data-reactid="41"]').html();            // Finding element by data-react-id is not feasible;
    // Generally its not a good idea to depend on auto-generated id (41 in this case).
    // Try this: $("[itemprop='price']").html()
    // To test: try the same code on different home details pages (HDP). Your code will not work on this page:
    // https://www.redfin.com/IL/Naperville/549-Fessler-Ave-60565/home/18070192
    price=price.replace(/,/g,"");                             // Only needed to replace "," not "$" sign as directly took value from data-react-id;
    price=+price;                                             // Converting string to integer;
    console.log("Price : "+price);                            // Display Value of Price Found from HTML;
    var rent=1800;                                            // Assuming rent to be $1800;
    var grm=(price/(rent*12)).toFixed(1);                     // Calculating GRM ;
    console.log("GRM : $"+grm);                               // Display GRM ;
    console.log("Ended !");
},4000); // Setting 4 secs delay for page to load all HTML Values;