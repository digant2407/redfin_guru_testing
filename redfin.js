// ==UserScript==
// @name         Redfin Rent Calculator
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.redfin.com/WA/*
// @grant        none
// @require      http://code.jquery.com/jquery-3.1.1.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.js
// ==/UserScript==

(function($) {
    $.fn.goTo = function() {                                   //$.fn   ???
        $('html, body').animate({                              //Predefined Jquery animate function;
            scrollTop: $(this).offset().top + 'px'             //To automatically scroll to GRM Table;
        }, 'fast');
        return this; // for chaining...                        
    };
})(jQuery);                                                    // (jQuery)  ???

// Create variables;
var pni = 0;                     
var escrow = 0;                  
var rent = 1800;             
var vacancy = 0;                
var maintenance = 0;            
var caprate = 0;                
var totalExpenses = 0;           
var price = 0;                  
var cashflow = 0;               

function setup() {                            
    var datarowElements = $(".dot-value-container .dot-value-row");     // ???    
    $.each(datarowElements, function(key, element) {                    // ???
        var html = $($(element).children("span.value")[0]).html();      //Get string in price's place;
        if(!html) { return; }                                           //If no HTML is found return NULL;

        var value = html.replace(/\$/g, "").replace(/,/g, "");          //Replace $ and , in String with "";
        if(isNaN(value)) { return; }                                    //Check if String doesn't contain and other char than int;

        if(_.includes(element.innerHTML, "Principal and Interest")) {   //Check if it is actually PNI;
            pni = +value;                                               //Convert String of PNI to value;
        } else {
            escrow += +value;                                           //Escrow = Escrow + convertedValue;
        }
    });
    price = $('div[data-rf-test-name="mc-homePrice"] input.dijitInputInner.currency').val().replace(/\$/g, "").replace(/,/g, "");
}

function updateDisplay() {
    // Update the values;
    $("#escrow").html("$"+escrow.toFixed(0));
    $("#rent").val(rent);
    $("#vacancy").html("$"+vacancy.toFixed(0));
    $("#maintenance").html("$"+maintenance.toFixed(0));
    $("#total-expenses").html("$"+totalExpenses.toFixed(0));
    $("#caprate").html(caprate+"%");
    $("#grm").html((price/(12*rent)).toFixed(1));
}
// Actual rent calculation;
function recalculate(newRent) {
    rent = newRent;
    vacancy = +(rent/12).toFixed(0);
    maintenance = +(rent/12).toFixed(0);
    totalExpenses = escrow + pni + vacancy + maintenance;
    caprate = ((12*100*(rent-escrow-vacancy-maintenance))/price).toFixed(1);
    updateDisplay();
}

setTimeout(function() {
    'use strict';                                    // Use strict Javascript i.e not allow undeclared variables;
    console.log('Rent Calculator started...');       //Print console;
    $(".Section.MortgageCalculatorSection").goTo();   //goto class .Section.MortgageCalculatorSection 

    // Show these values in the table 
    // Create a display in class .dot-value-container;
    $(".dot-value-container").append('<div class="dot-value-row"><div class="DotTitle lime"><span>&nbsp;</span></div></div>');
    $(".dot-value-container").append('<div class="dot-value-row"><div class="DotTitle lime"><span class="title">Escrow</span></div><span id="escrow" class="value"></span></div>');
    $(".dot-value-container").append('<div class="dot-value-row"><div class="DotTitle lime"><span class="title">Vacancy</span></div><span id="vacancy" class="value"></span></div>');
    $(".dot-value-container").append('<div class="dot-value-row"><div class="DotTitle lime"><span class="title">Maintenance</span></div><span id="maintenance" class="value"></span></div>');
    $(".dot-value-container").append('<div class="dot-value-row"><div class="DotTitle lime"><span class="title">Total</span></div><span id="total-expenses" class="value"></span></div>');
    $(".dot-value-container").append('<div class="dot-value-row"><div class="DotTitle lime"><span class="title">Rent</span></div><input type="text" id="rent" class="value" value="" style="line-height:1.2em;border-top:0;border-right:0;border-left:0;text-align:right;" /></div>');
    $(".dot-value-container").append('<div class="dot-value-row"><div class="DotTitle lime"><span class="title">Cap Rate</span></div><span id="caprate" class="value"></span></div>');
    $(".dot-value-container").append('<div class="dot-value-row"><div class="DotTitle lime"><span class="title">Gross Rent Multiplier</span></div><span id="grm" class="value"></span></div>');

    setup();                    // Call function setup;
    $("#rent").val(rent);       //Put the value of rent var in input of id rent ;
    recalculate(rent);          //Calculate rent through initialised value; 
    $("#rent").on('keyup', function(event) {     // Recalculate rent when value of input of id rent is changed; 
        recalculate(event.target.value);         // Call recalculate function;
    });
}, 4000);                       // Set timeout to 4 sec to let page reload ;
